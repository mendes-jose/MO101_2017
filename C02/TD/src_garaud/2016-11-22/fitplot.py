#! /usr/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

############ PLOT ##################

data=np.loadtxt("expe.dat")
## toutes sur une courbe, ce n'est pas lisible
# plt.plot ( data[:,0], data[:,2], 'ro',
#            data[:,0], data[:,3], 'bs',
#            data[:,0], data[:,4], 'g^')
# plt.show()

plt.subplot(1,3,1)   # rows, columns, subplotId
plt.plot(data[:,0], data[:,1], 'ro')
plt.xlabel("time")
plt.ylabel("$\epsilon_{11}$")


plt.subplot(1,3,2)   # rows, columns, subplotId
plt.plot(data[:,1], data[:,2], 'ro')
plt.xlabel("$\epsilon_{11}$")
plt.ylabel("force")

plt.subplot(1,3,3)
plt.plot(data[:,1], data[:,3], 'bs')
plt.xlabel("$\epsilon_{11}$")
plt.plot(data[:,1], data[:,4], 'g^')
plt.ylabel("$\sigma$ et $\sigma_{11}$")

plt.show()



############ FIT 1e version, avec polyfit ##################

from numpy import polyfit
NPOINTS=5
X = data[:NPOINTS,1]
Y = data[:NPOINTS,3]

a,b=polyfit(X,Y, deg=1)

plt.plot(data[:,1], data[:,3], 'bs')
plt.xlabel("$\epsilon_{11}$")
plt.ylabel("$\sigma$")

# plot la fonction fittee
Y = a*X+b
plt.plot(X,Y,'k-')

#  en theorie, b=0, ca donnerait cette courbe :
plt.plot(X,a*X,'k-')

# Affichage du graphique et du module d'elasticite trouve
plt.annotate("y=%f*x+%f"%(a,b), xy=(X[4]+0.001,Y[4]))
plt.annotate("y=%f*x+%f"%(a,0), xy=(X[4]+0.001,Y[4]-b))

plt.show()



############ FIT 2e version, avec leastsq ##################

from scipy.optimize import leastsq
NPOINTS=5
def fitfunc(E,x):
    "La fonction (lineaire) a fitter"
    return E*x
def errfunc(E,x,y):
    "La fonction d'erreur : renvoye la distance entre fitfunc et y"
    return fitfunc(E,x) - y

X = data[:NPOINTS,1]
Y = data[:NPOINTS,3]

E, success = leastsq( errfunc,     # fonction a minimiser
                      0.,          # initial guess
                      args=(X,Y)   # donnees a fitter
                      )
if not (success in (1,2,3,4) ) :   # cf. la doc le la fonction leastsq
    raise StandardError("L'optimisation a rate ... ")

# plot la fonction fittee
X = data[0:NPOINTS,1]
Y = fitfunc(E,X)

plt.plot(data[:,1], data[:,3], 'bs')
plt.xlabel("$\epsilon_{11}$")
plt.ylabel("$\sigma$")
plt.plot(X,Y,'k-')

# Affichage du graphique et du module d'elasticite trouve
plt.annotate("E=%f"%E, xy=(X[4]+0.001,Y[4]))

plt.show()

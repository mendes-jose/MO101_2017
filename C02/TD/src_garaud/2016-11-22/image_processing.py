import numpy as np
#import matplotlib.pyplot as plt
from skimage import io, morphology
from scipy import ndimage
from skimage.measure import regionprops , find_contours
from skimage.filter import threshold_adaptive

imgfile="specimen.bmp"

img=io.imread(imgfile, as_grey=False, plugin=None, flatten=None)
io.imshow(img)
io.show()

block_size=200
binary_thresh=-38
binary_adaptive = threshold_adaptive(img, block_size=block_size, offset=binary_thresh)
io.imshow(binary_adaptive)
io.show()

from_window=10
binary_adaptive[1:from_window,:]=0
binary_adaptive[-1*from_window::,:]=0
binary_adaptive[:,1:from_window]=0
binary_adaptive[:,-1*from_window::]=0

io.imshow(binary_adaptive)
io.show()

size_se=7
Dila_1=morphology.binary_dilation(binary_adaptive,morphology.disk(size_se))
io.imshow(Dila_1)
io.show()

Ero_1=morphology.binary_erosion(Dila_1,morphology.disk(size_se))
io.imshow(Ero_1)
io.show()


min_size=160000
cleaned_image= morphology.remove_small_objects(Ero_1>0, min_size=min_size, connectivity=8)
io.imshow(cleaned_image)
io.show()
# filled_hole=cleaned_image
filled_hole=ndimage.binary_fill_holes(cleaned_image, structure=morphology.disk(2))
io.imshow(filled_hole)
io.show()

# To smooth border
border_smooth=60

Dila_2=morphology.binary_dilation(filled_hole,morphology.disk(border_smooth))
io.imshow(Dila_2)
io.show()


Ero_2=morphology.binary_erosion(Dila_2,morphology.disk(border_smooth))
io.imshow(Ero_2)
io.show()

gap_to_border=20
imgBW=morphology.binary_erosion(Ero_2,morphology.disk(gap_to_border))
io.imshow(imgBW)
io.show()



io.imshow(img*imgBW)
io.show()

io.imshow(img-img*imgBW)
io.show()

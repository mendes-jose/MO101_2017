import numpy as np
from scipy import misc
import matplotlib.pyplot as plt


face = misc.face()
#print (face)
print (face.shape)

# display image
plt.imshow(face)
plt.show()


# display the red channel
faceRed=np.copy(face)
faceRed[:,:,1]=0
faceRed[:,:,2]=0
plt.imshow(faceRed)
plt.show()
# display the green channel
faceGreen=np.copy(face)
faceGreen[:,:,0]=0
faceGreen[:,:,2]=0
plt.imshow(faceGreen)
plt.show()
# display the blue channel
faceBlue=np.copy(face)
faceBlue[:,:,0]=0
faceBlue[:,:,1]=0
plt.imshow(faceBlue)
plt.show()


# mirror image
faceMirror=face[:,::-1,:]
plt.imshow(faceMirror)
plt.show()

# convert to greyscale
R=face[:,:,0]
G=face[:,:,1]
B=face[:,:,2]

# average formula:
faceGrey1=(R+G+B)/3   # won't work: int8 overflow
third=1./3
faceGrey1=third*R + third*G + third*B # possible workaround
plt.imshow(faceGrey1, cmap='gray')
plt.show()

# luminosity formula:
faceGrey2=0.21* R + 0.72* G + 0.07* B
plt.imshow(faceGrey2, cmap='gray')
plt.show()

# lightness formula
faceGrey3=(np.maximum(1.0*R, 1.0*G) + np.minimum(1.0*R, 1.0*G)) / 2
plt.imshow(faceGrey3, cmap='gray')
plt.show()


# extract the face
radius=256
x0,y0=666,276

dy,dx=faceGrey1.shape
mask=np.zeros((dy, dx), dtype=np.uint8)
y,x=np.mgrid[0:dy,0:dx]
mask[ ( (x-x0)**2 + (y-y0)**2 ) < radius**2 ] = 1

faceGrey1 *= mask
plt.imshow(faceGrey1, cmap='gray')
plt.show()

# crop
cropped = faceGrey1[y0-radius:y0+radius,x0-radius:x0+radius]
plt.imshow(cropped, cmap='gray')
plt.show()

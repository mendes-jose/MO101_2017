import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

data=np.loadtxt("expe.dat")

fig = plt.figure()

plt.subplot(221)
ax = fig.gca()
ax.plot(data[:,0], data[:,1], 'rs')
plt.ylabel("$\epsilon_{11}$")
plt.xlabel("time")

plt.subplot(222)
ax = fig.gca()
ax.plot(data[:,0], data[:,2], 'gs')
plt.ylabel("$force$")
plt.xlabel("time")

plt.subplot(223)
ax = fig.gca()
ax.plot(data[:,0], data[:,3], 'bs')
plt.ylabel("$\sigma$")
plt.xlabel("time")

plt.subplot(224)
ax = fig.gca()
ax.plot(data[:,0], data[:,4], 'ks')
plt.ylabel("$\sigma_{11}$")
plt.xlabel("time")

plt.show()

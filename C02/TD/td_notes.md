# TD 02

### Intro

#### Les differents façon d'appeler l'interpreteur python

- python3 program.py
- python3 >> program.py
- chmod +x program (containing #!/usr/bin/env python3, opt: # -*- coding: utf-8 -*-)

Use `help()`!

#### Les objectifs des TDs

Le dernier TD nous avons vu Python comme une **langage de script** (scripting language).

Le but c'était d'automatiser des operations, notamment, la gestion de fichiers, l'indentification de expression régulieres dans un texte, le téléchargement d'un nombre de fichiers images sur internet et emuler une command linux

Ce nouveau TD a comme but montrer l'utilisation **scientifique** de python (comparable à Matlab).

#### Exo 1. Essais mécaniques (Curve plotting and fitting)
Les essais mécaniques sont des expériences dont le but est de caractériser les lois de comportement des matériaux.

- Epsilon = strain, déformation élastique
- Sigma = stress, force

#### Exo 2. Image processing with numpy/scipy/matplotlib

#### Exo 3. Going further: Image processing using skimage
